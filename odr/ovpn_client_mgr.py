import time
import random
from typing import Dict, Callable, Optional, List, Awaitable
from functools import partial
import asyncio

from prometheus_client import Gauge
import structlog

from .realmdata import RealmData
from .ovpn_client import OvpnClient
from .ovpn import OvpnServer, OvpnClientConnData
from .dhcprequestor import DhcpAddressResponse


M_OVPN_CLIENTS_CONNECTED = Gauge("ovpn_clients_connected", "number of currently connected clients",
                                 labelnames=["server"])

class OvpnClientManager:
    """Manages a list of all clients currently connected to all known OpenVPN
    servers.  Takes care of regularly refreshing the client's DHCP leases.

    Periodically polls the OpenVPN servers to sync the list of connected
    clients.

    Note: Some clients might still be tracked by the manager, but already marked
          as killed.  These zombies should be collected as soon as the client-
          disconnect hook gets processed by OpenVPN or as soon as the next
          client list poll completes.
    """

    def __init__(
        self,
        realms_data: Dict[str, RealmData],
        parse_username_clb: Callable[[str], Optional[Dict[str, str]]],
        servers: Dict[str, OvpnServer],
        secret: str,
        refresh_lease_clb: Callable[..., Awaitable[DhcpAddressResponse]],
        send_request_clb: Callable[..., Awaitable[DhcpAddressResponse]],
        sync_interval: int = 60,
    ) -> None:
        """\
        @param realms_data: Map of realm names to realm data structures.
        @param parse_username_clb: Call-back to parse the full_username into
            its components.
        @param servers: List of OpenVPN servers to query.
        @param refresh_lease_clb: Callback for refreshing a DHCP lease.
        @param sync_interval: Intervall in which to poll the servers.
        """
        self._realms_data = realms_data
        self._parse_username = parse_username_clb
        self._servers = servers
        self._refresh_lease = refresh_lease_clb
        self._send_request = send_request_clb
        self._secret = secret
        self._sync_interval = sync_interval

        self._log = structlog.get_logger('ovpnclientmgr')
        self._clients_by_username = {}  # type: Dict[str, OvpnClient]

        self._clients_by_server = {}
        for server in self._servers.values():
            self._clients_by_server[server] = {}

        self._sync_task = asyncio.create_task(self.sync_clients())

    def create_client(self, **kwargs) -> OvpnClient:
        """Create and keep track of an OpenVPN client connection.  All keyword
        arguments are passed on to OvpnClient's constructor.
        @return: Returns the newly created OvpnClient instance.
        """
        client = OvpnClient(
            refresh_lease_clb=self._refresh_lease,
            send_request_clb=self._send_request,
            secret=self._secret,
            **kwargs
        )
        self._add_client(client)
        return client

    def _add_client(self, client: OvpnClient) -> None:
        """Add a client, based on a completed and successful DHCP request.
        """
        if client.full_username in self._clients_by_username:
            self._log.info(
                'replacing client connection in client list with freshly connected '
                ' client instance',
                client=client,
            )
            self._del_client(self._clients_by_username[client.full_username])
        else:
            self._log.debug('adding new client instance', client=client)

        self._clients_by_username[client.full_username] = client
        self._clients_by_server[client.server][client.full_username] = client

    async def sync_clients(self) -> None:
        """Syncs the client list with the client lists of each OpenVPN server.

        Any client connected to the server but not listed by us needs to be
        added to our client list.  Those client's leases need to be refreshed
        soon, as their last refresh time is unknown to us.

        Any clients that are listed by us but no longer listed by the OpenVPN
        server are removed from our list.  They have been disconnected.
        """
        while True:
            for server in self._servers.values():
                # Asynchronously retrieve the list of clients against which to sync.
                server.poll_client_list(partial(self._sync_clients_with, server=server))

            await asyncio.sleep(self._sync_interval)

    def _sync_clients_with(self, client_data_list: List[OvpnClientConnData],
                           server: OvpnServer) -> None:
        """Called per-server as soon as the server's client data list has
        been retrieved.  Performs the actual processing as documented for
        sync_clients().
        """
        log = self._log.bind(server=server.name)

        if client_data_list is None:
            log.error('syncing the client list failed')
            return

        M_OVPN_CLIENTS_CONNECTED.labels(server=server.name).set(len(client_data_list))

        client_data_by_username = {}

        for client_data in client_data_list:
            log.debug(
                'client_data: "%s" with "%s" and "%s"',
                client_data.common_name,
                client_data.virtual_address_ipv4,
                client_data.virtual_address_ipv6,
            )
            if client_data.virtual_address_ipv4 is None and client_data.virtual_address_ipv6 is None:
                # Connection hasn't been fully established yet.  Skip it.
                continue

            client_data_by_username[client_data.common_name] = client_data

            if client_data.common_name in self._clients_by_username:
                client = self._clients_by_username[client_data.common_name]
                if client.server != server:
                    # The client has jumped servers.  Remove it from the list.
                    log.debug(
                        'cleaning up: client %s has moved to server "%s"',
                        client,
                        server,
                    )
                    self._del_client(client)

            if client_data.common_name not in self._clients_by_username:
                # New client!  Assume pessimistic last lease update time.  We're
                # probably recovering from a daemon restart.
                # IPv4 address needed for dhcp lease refresh.
                self._create_detected_client(
                    client_data.common_name, server, client_data.virtual_address_ipv4
                )

        for client in list(self._clients_by_server[server].values()):
            if client.full_username not in client_data_by_username:
                # The client has been disconnected.
                if not client.iszombie:
                    self._log.debug(
                        'client was disconnected in the mean-while',
                        client=client,
                    )
                else:
                    self._log.debug('removing zombie client', client=client)
                self._del_client(client)

    def _del_client(self, client) -> None:
        """Kills a client instance and removes it from the manager's knowledge.
        In case the client has some pending operations, it might live on for
        some time.
        @param client: The client instance to kill and forget.
        """
        client.kill()
        del self._clients_by_username[client.full_username]
        del self._clients_by_server[client.server][client.full_username]

    def client_disconnected(self, full_username: str, server: OvpnServer) -> None:
        """Called when a client was disconnected.
        """
        if server not in self._clients_by_server:
            self._log.error(
                'attempted to disconnect user from unkown server "%s"',
                server=server,
                user=full_username,
            )
            return
        server_clients = self._clients_by_server[server]

        if full_username not in server_clients:
            self._log.error('attempted to disconnect user', user=full_username)
            return
        client = server_clients[full_username]

        if not client.iszombie:
            self._log.debug('removing zombie client', client=client)
        else:
            self._log.debug('client disconnected', client=client)
        self._del_client(client)

    def _create_detected_client(self, full_username: str, server: OvpnServer,
                                leased_ip_address: str) -> None:
        """Create a client instance without knowledge of the last DHCP lease
        refresh time.  Therefore, the client's next lease update time is set to
        "soon".

        @param full_username: The full username of the connected client.
        @param server: The server instance the client is connected to.
        """
        self._log.debug('detected client', user=full_username)
        ret = self._parse_username(full_username)
        if ret is None:
            self._log.warning('parsing username failed', user=full_username)
            server.disconnect_client(full_username)
            return
        realm = ret['realm']

        if realm not in self._realms_data:
            self._log.warning('unknown realm "%s" for user "%s"', realm, full_username)
            server.disconnect_client(full_username)
            return
        realm_data = self._realms_data[realm]

        async def refresh_existing_lease(client: OvpnClient):
            # We have no idea when the last refresh occured for this client,
            # but it's unlikely to be needed immediately.  Spread out the
            # requests a bit.
            await asyncio.sleep(random.uniform(0, 10))
            await client.track_existing_lease(leased_ip_address)

        client = self.create_client(
            server=server,
            full_username=full_username,
            realm_data=realm_data,
        )

        if leased_ip_address is not None:
            asyncio.create_task(refresh_existing_lease(client))
