# ovpn.py - Provides OpenVPN constants.
#
# Copyright © 2010 Fabian Knittel <fabian.knittel@lettink.de>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import socket
import sys
from typing import Optional, TextIO, Callable, List
import asyncio

import structlog

from odr.linesocket import LineSocket
from odr.queue import StateQueue
from odr.socketloop import SocketLoop

CC_RET_FAILED = 0
CC_RET_SUCCEEDED = 1
CC_RET_DEFERRED = 2


def write_deferred_ret_file(fp: TextIO, val: object) -> None:
    """Write one of the deferral values to the deferred return value file.

    @param fp: File pointer of the deferred return value file.
    @param val: One of the CC_RET_* constants.
    """
    fp.seek(0)
    fp.write(str(val))
    fp.flush()
    os.fsync(fp.fileno())


def determine_daemon_name(script_name: str) -> Optional[str]:
    """We identify the OpenVPN server instance calling us by either looking at
    an environment variable or by trying to deduce the name from the way we
    were called.

    @param script_name: The regular base filename of this Python script.
    @return: Returns the OpenVPN server instance name or None.
    """

    if 'daemon_name' in os.environ:
        return os.environ['daemon_name']

    b = os.path.basename(sys.argv[0])
    if b.startswith(script_name + '_'):
        return b[len(script_name) + 1 :]

    return None


class OvpnClientConnData:
    """Represents a single client connection within an OpenVPN server's client
    list.
    """

    def __init__(self, **kwargs) -> None:
        self.common_name = kwargs.get('common_name', None)
        self.virtual_address_ipv4 = kwargs.get('virtual_address_ipv4', None)
        self.virtual_address_ipv6 = kwargs.get('virtual_address_ipv6', None)
        self.server = kwargs.get('server', None)

    def __str__(self) -> str:
        return f'{self.common_name} on {self.server}'

    def __repr__(self) -> str:
        return "<OvpnClientConnData common_name=%s, ...>" % self.common_name


class OvpnServer:
    """Represents a single OpenVPN server and allows communication with the
    server (via the management console).
    """

    def __init__(self, sloop: SocketLoop, name: str, socket_fn: str) -> None:
        """\
        @param sloop: Instance of the socket loop.
        @param name: Freely choosable, unique identifier of the server.
        @param socket_fn: Path to the management console's UNIX socket.
        """
        self._sloop = sloop
        self._name = name
        self._socket_fn = socket_fn

        self.log: structlog.BoundLogger = structlog.get_logger('ovpnsrv').bind(name=name, address=socket_fn)
        self._socket = None  # type: Optional[LineSocket]
        self._cmd_state = StateQueue(idle_state=_OvpnIdleState())

        self.connect_to_mgmt()

    @property
    def connected(self) -> bool:
        return self._socket is not None

    def connect_to_mgmt(self) -> None:
        if self.connected:
            self.log.debug('replacing connection to management console')
            self.close_mgmt()

        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        try:
            sock.connect(self._socket_fn)
        except OSError as e:
            self.log.error('connection failed', exception=e)
            sock.close()
            return

        self.log.debug(
            'connected', server=self.name, address=self._socket_fn
        )
        self._socket = LineSocket(sock)
        self._sloop.add_socket_handler(self)

        self._cmd_state.add(_OvpnWaitConnectState(self._on_connected))

    def close_mgmt(self) -> None:
        self._sloop.del_socket_handler(self)
        self._socket.close()
        self._socket = None
        self._cmd_state.clear()

    def _on_connected(self, hello_msg) -> None:
        if not hello_msg.startswith(b'>INFO:'):
            self.log.error(
                'connection failed', hello=hello_msg
            )
            self.close_mgmt()
            return
        self.log.debug('connected')

    def __del__(self) -> None:
        if self.connected:
            # Note:  We're obviously no longer managed by the sloop, otherwise
            # we wouldn't be getting collected.  Therefore no need to remove us
            # from the sloop.
            self._socket.close()

    # this was implemented on the original as __cmp__. I can't find any use of it yet.
    def __eq__(self, other) -> bool:
        return self.name == other.name

    def __hash__(self) -> int:
        return hash(self._name)

    def __str__(self) -> str:
        return self.name

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__} {self._name!r}>"

    @property
    def name(self) -> str:
        return self._name

    @property
    def socket(self) -> Optional[LineSocket]:
        """@return: Returns the listening socket.
        """
        return self._socket

    def handle_socket(self) -> None:
        lines = self._socket.recvlines()
        if lines is None:
            # EOF - clean-up.
            self.log.error('received EOF')
            self.close_mgmt()
            return

        for line in lines:
            # Feed each line to the current state.  If the state indicates
            # completion, move to next state.
            if not self._cmd_state.current.handle_line(line):
                self._cmd_state.current_done()

    def _send_cmd(self, cmd) -> None:
        try:
            self._socket.send(cmd.replace(b'\n', b'\\n') + b'\n')
        except BrokenPipeError as ex:
            self.log.error('socket closed', exception=ex)
            self.close_mgmt()

    def disconnect_client(self, common_name) -> None:
        """Disconnects the specified client from this OpenVPN server instance.

        @param common_name: Common name of the client that should be
                disconnected.
        """
        if not self.connected:
            self.log.debug(
                'ignoring disconnect_client call, no active connection',
            )
            return
        self.log.debug(
            'disconnecting client', cn=common_name
        )
        self._cmd_state.add(
            _OvpnDisconnectClientsState(self, common_name, lambda res: None)
        )

    def poll_client_list(self, list_done_clb) -> None:
        """Polls the list of clients connected to this server.  On completion,
        the callback is called with the complete list as parameter.  In case
        of an error, the callback is called with None instead of the list.

        @param: list_done_clb The callback function to call on completion or
                error.
        """
        if not self.connected:
            self.log.debug(
                'ignoring poll_client_list call, no active connection',
            )
            return
        self.log.debug('polling user list')
        self._cmd_state.add(_OvpnListClientsState(self, list_done_clb))


class _OvpnIdleState:
    """The default state of the management console.  Takes all lines, ignores
    them and wants to continue forever.
    """

    def handle_line(self, line: bytes) -> bool:
        return True


class _OvpnWaitConnectState:
    """Waits for an OpenVPN management socket to to connect.
    """

    def __init__(self, done_clb: Callable[[str], None]) -> None:
        self._done = done_clb

    def handle_line(self, line: bytes) -> bool:
        self._done(line)
        return False


class _OvpnListClientsState:
    """Uses an OpenVPN management socket to asynchronously request the client
    list.
    """

    def __init__(self, ovpn,
                 list_done_clb: Callable[[List[OvpnClientConnData]], None]) -> None:
        self._ovpn = ovpn
        self._list_done = list_done_clb

        self._clients = []
        self._ovpn._send_cmd(b'status 2')

    def _parse_client_line(self, line: bytes) -> None:
        cl = OvpnClientConnData(server=self._ovpn)
        d = line.decode("utf-8").split(',')
        cl.common_name = d[1]
        if d[3] != '':
            cl.virtual_address_ipv4 = d[3]
        else:
            cl.virtual_address_ipv4 = None
        if d[4] != '':
            cl.virtual_address_ipv6 = d[4]
        else:
            cl.virtual_address_ipv6 = None
        self._clients.append(cl)

    def handle_line(self, line: bytes) -> bool:
        if line.startswith(b'CLIENT_LIST,'):
            self._parse_client_line(line)
        elif line == b'END\n':
            self._list_done(self._clients)
            return False
        return True


class _OvpnDisconnectClientsState:
    """Uses an OpenVPN management socket to asynchronously disconnect a client.
    """

    def __init__(self, ovpn, common_name: str, done_clb: Callable[[bool], None]) -> None:
        self._done = done_clb
        ovpn._send_cmd(b'kill "%s"' % common_name.encode("ascii"))

    def handle_line(self, line: bytes) -> bool:
        if line.startswith(b'SUCCESS:'):
            self._done(True)
            return False
        elif line.startswith(b'ERROR:'):
            self._done(False)
            return False
        return True


async def supervise_server(
    server: OvpnServer, timeout: float
) -> None:
    """Makes sure the associated OpenVPN server has an active management
    connection.
    """

    log = structlog.get_logger('ovpnserversup')
    log.debug('watching server connection', server=server)

    while True:
        try:
            if not server.connected:
                server.connect_to_mgmt()

            await asyncio.sleep(timeout)
        except asyncio.CancelledError:
            log.debug('no longer watching server connection', server=server)
            raise

