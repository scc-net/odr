import time
from typing import Optional, Awaitable, Callable
from ipaddress import IPv4Address, IPv4Network
from asyncio import Future
import asyncio
import datetime
import hashlib

import structlog
from prometheus_client import Counter

from .dhcprequestor import DhcpAddressResponse
from . import ovpn
from .realmdata import RealmData
from .ovpn_config import OvpnConf, add_realm_config, add_ipv4_config, add_ipv6_config
from ipaddress import IPv4Network, IPv4Address, IPv6Network

M_DHCP_REQUEST_COUNT = Counter(
    "dhcp_request_count", "number of attempted dhcp requests", ("realm",)
)
M_DHCP_REQUEST_FAIL_COUNT = Counter(
    "dhcp_request_fail_count", "number of failed dhcp requests", ("realm",)
)
M_DHCP_REFRESH_COUNT = Counter(
    "dhcp_refresh_count", "number of attempted dhcp refreshes", ("realm",)
)
M_DHCP_REFRESH_FAIL_COUNT = Counter(
    "dhcp_refresh_fail_count", "number of failed dhcp refreshes", ("realm",)
)

class OvpnClient:
    """Represents an OpenVPN client connected to a specific OpenVPN server
    instance.
    """

    def __init__(
        self,
        refresh_lease_clb: Callable[..., Awaitable[DhcpAddressResponse]],
        send_request_clb: Callable[..., Awaitable[DhcpAddressResponse]],
        full_username: str,
        secret: str,
        server: ovpn.OvpnServer,
        realm_data: RealmData,
    ) -> None:
        self._refresh_lease = refresh_lease_clb
        self._send_request = send_request_clb
        self._secret = secret
        self.full_username = full_username
        self.server = server
        self._realm_data = realm_data
        self._dhcp_response = None

        self._log = structlog.get_logger('ovpnclient').bind(
            name=full_username, server=server)
        self._killed = False

    def __str__(self) -> str:
        return f'{self.full_username} on {self.server}'

    def __repr__(self) -> str:
        return "<OvpnClient(common_name={}, server={}, ...)>".format(
            self.full_username, self.server
        )

    def track_lease(self) -> None:
        """Start keeping track of the DHCP lease time and make sure the lease
        is refreshed early enough.
        """
        if self._dhcp_response is None:
            self._log.error(
                'attempted to track lease but no lease available',
            )
            return
        rebinding_delta = self._dhcp_response.rebinding_delta
        self._log.debug("tracking lease", rebinding_delta=rebinding_delta)
        loop = asyncio.get_running_loop()
        loop.call_later(rebinding_delta, self.handle_timeout)

    async def track_existing_lease(self, old_ip: str):
        target_addr = IPv4Network(self._realm_data.subnet_ipv4).network_address
        self._dhcp_response = await self._refresh_lease(
            client_identifier=self.full_username,
            device=self._realm_data.dhcp_listening_device,
            local_ip=self._realm_data.dhcp_listening_ip,
            server_ips=self._realm_data.dhcp_server_ips,
            target_addr=target_addr,
            client_ip=old_ip,
            lease_time=self._realm_data.expected_dhcp_lease_time,
        )
        self._log.debug('DHCP lease rebound: %s', repr(self._dhcp_response))
        self.track_lease()

    def kill(self) -> None:
        """Disable the client.  Although any pending activities will continue,
        no new activities will be started.
        """
        self._killed = True

    @property
    def iszombie(self) -> bool:
        """Has this client instance been killed?
        @return: Returns True if the instance has been killed, otherwise False.
        """
        return self._killed

    def handle_timeout(self) -> None:
        """Called as soon as the rebinding timeout occurs.
        """
        asyncio.create_task(self.do_refresh())

    async def generate_config(self) -> OvpnConf:
        conf = OvpnConf()
        add_realm_config(conf, self._realm_data)

        if self._realm_data.subnet_ipv4 and self._realm_data.dhcp_server_ips:
            req = await self.do_request()
            self._log.debug('DHCP request succeeded: %s', repr(req))
            self._dhcp_response = req
            add_ipv4_config(conf, self._realm_data, req)

        if self._realm_data.subnet_ipv6:
            today = str(datetime.date.today())
            hasher = hashlib.sha256()
            hasher.update((self.full_username + today + self._secret).encode('utf-8'))
            user_hash = hasher.digest()[:8]

            add_ipv6_config(conf, self._realm_data, user_hash)

        return conf

    async def do_request(self) -> DhcpAddressResponse:
        target_addr: Optional[IPv4Address]
        realm_data = self._realm_data

        target_addr = IPv4Network(realm_data.subnet_ipv4).network_address
        try:
            M_DHCP_REQUEST_COUNT.labels(realm_data.name).inc()
            return await self._send_request(
                client_identifier=self.full_username,
                device=realm_data.dhcp_listening_device,
                local_ip=realm_data.dhcp_listening_ip,
                server_ips=realm_data.dhcp_server_ips,
                target_addr=target_addr,
                lease_time=realm_data.expected_dhcp_lease_time,
            )
        except Exception:
            M_DHCP_REQUEST_FAIL_COUNT.labels(self._realm_data.name).inc()
            self._log.exception('DHCP request failed')
            raise

    async def do_refresh(self) -> None:
        if self.iszombie:
            return

        if (self._dhcp_response is not None and
                self._dhcp_response.lease_timeout <= time.time()):
            self._log.warning(
                'lease already expired, disconnecting client',
                timeout=self._dhcp_response.lease_timeout,
            )
            self.server.disconnect_client(self.full_username)
            return

        target_addr = IPv4Network(self._realm_data.subnet_ipv4).network_address

        M_DHCP_REFRESH_COUNT.labels(self._realm_data.name).inc()

        try:
            res = await self._refresh_lease(
                client_identifier=self.full_username,
                device=self._realm_data.dhcp_listening_device,
                local_ip=self._realm_data.dhcp_listening_ip,
                server_ips=self._realm_data.dhcp_server_ips,
                target_addr=target_addr,
                client_ip=self._dhcp_response.ip_address,
                lease_time=self._realm_data.expected_dhcp_lease_time,
            )
        except Exception as e:
            # if the lease could not be renewed, the client needs to be
            # disconnected to establish a new one
            self._log.exception('DHCP refresh request failed')
            M_DHCP_REFRESH_FAIL_COUNT.labels(self._realm_data.name).inc()
            self.server.disconnect_client(self.full_username)
            return

        if self.iszombie:
            return

        self._log.debug('DHCP refresh request succeeded: %s', repr(res))
        self._dhcp_response = res
        rebinding_delta = res.rebinding_delta
        # schedule a new renewal
        loop = asyncio.get_running_loop()
        loop.call_later(rebinding_delta, self.handle_timeout)


