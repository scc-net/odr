#!/usr/bin/env python3

# odrd -- OpenVPN DHCP Requestor daemon
#
# Copyright © 2010 Fabian Knittel <fabian.knittel@avona.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import grp
import logging
import os
import pwd
import signal
import sys
import weakref
import socket
from configparser import ConfigParser
from optparse import OptionParser
from typing import Dict, List, Any
from asyncio import Future
import asyncio
import structlog

import prctl

from prometheus_client import start_http_server

from prometheus_client import start_http_server

import odr.dhcprequestor
import odr.listeningsocket
import odr.ovpn as ovpn

from .cmdconnection import CommandConnectionListener
from .parse import ParseUsername
from .config import cfg_iterate, split_cfg_list
from .socketloop import SocketLoop
from .realmdata import read_realms, RealmData
from .ovpn_client_mgr import OvpnClientManager
from .ovpn_cmdconnection import OvpnCmdConn
from .dhcprequestor import DhcpAddressResponse, DhcpAddressRequestorManager

CONFIG_FILE = '/etc/odr.conf'

def user_to_uid(user: str) -> int:
    """Transforms a user to a UID.  In case the user is already a UID, the
    UID is passed through unchanged.
    @param user: The user as string.  It may either be the username or the
        user's UID.
    @return: Returns the UID as integer.
    """
    try:
        uid = int(user)
    except ValueError:
        try:
            uid = pwd.getpwnam(user).pw_uid
        except KeyError:
            logging.critical('could not resolve user "%s", exiting', user)
            sys.exit(1)
    return uid


def group_to_gid(group: str) -> int:
    """Transforms a group to a GID.  In case the group is already a GID, the
    GID is passed through unchanged.
    @param group: The group as string.  It may either be the group name or the
        group's GID.
    @return: Returns the GID as integer.
    """
    try:
        gid = int(group)
    except ValueError:
        try:
            gid = grp.getgrnam(group).gr_gid
        except KeyError:
            logging.critical('could not resolve group "%s", exiting', group)
            sys.exit(1)
    return gid


def drop_caps(user: str=None, group: str=None, caps: List[str]=None) -> None:
    """Switches aways from UID 0 and full capabilities to a different user
    and a limited set of capabilities.  Child processes get none of the
    capabilities.
    @param user: The target user
    @param group: The target group
    @param caps: List of capabilities to retain.
    """
    if caps is None:
        caps = []

    if group is not None:
        # Switch to new GID.
        logging.debug('Switching to group %s', str(group))
        gid = group_to_gid(group)
        os.setgid(gid)
        os.setgroups([gid])

    if user is not None:
        # Retain all capabilities over UID switch.
        prctl.set_keepcaps(True)

        # Switch to new UID.
        logging.debug('Switching to user %s', str(user))
        os.setuid(user_to_uid(user))

    if len(caps) > 0:
        logging.debug('Restricting to capabilities "%s"', ', '.join(caps))
        # Some capabilities might be permitted but not effective, so explicitly
        # set them to effective here.
        for cap in caps:
            setattr(prctl.cap_effective, cap, True)
    else:
        logging.debug('Dropping all capabilities.')
    # Drop all capabilities except those listed in "caps".
    prctl.cap_effective.limit(*caps)
    prctl.cap_permitted.limit(*caps)
    # Child processes may not use our capabilities.
    prctl.cap_inheritable.limit()


def read_servers(cfg: ConfigParser, sloop: SocketLoop) -> Dict[str, ovpn.OvpnServer]:
    """Read all servers from the configuration file and connect to each of
    them.
    """
    servers = {}
    for sect, server_name in cfg_iterate(cfg, 'ovpn-server'):
        server = ovpn.OvpnServer(
            sloop, name=server_name, socket_fn=cfg.get(sect, 'mgmt_socket')
        )
        servers[server_name] = server
    return servers


def load_requestors(sloop: SocketLoop, requestor_mgr: DhcpAddressRequestorManager,
                    realms_data: Dict[str, RealmData]) -> bool:
    """Load all requestors, based on the existing realms.

    @param sloop: Socket loop instance.
    @param requestor_mgr: Requestor manager instance.
    @param realms_data: Dictionary of all existing realms.
    @return: Returns False in case an error occured while loading the
        requestors.  Otherwise returns True.
    """
    try:
        for realm_data in realms_data.values():
            if requestor_mgr.has_requestor(
                realm_data.dhcp_listening_device, realm_data.dhcp_listening_ip
            ):
                # Skip creating requestor, a previous realm already did that.
                continue

            requestor = odr.dhcprequestor.DhcpAddressRequestor(
                listen_address=realm_data.dhcp_listening_ip,
                listen_port=realm_data.dhcp_local_port,
                listen_device=realm_data.dhcp_listening_device,
            )
            sloop.add_socket_handler(requestor)
            requestor_mgr.add_requestor(requestor)
    except odr.listeningsocket.SocketLocalAddressBindFailed as ex:
        logging.error(
            'Could not bind to DHCP listening address %s:%d@%s',
            ex.args[0],
            ex.args[1],
            ex.args[2],
        )
        return False
    return True


def setup_logging(loglevel: int, use_syslog: bool = False) -> None:
    structlog.configure(
        processors=[
            structlog.stdlib.add_logger_name,
            structlog.stdlib.add_log_level,
            structlog.stdlib.PositionalArgumentsFormatter(),
            structlog.processors.StackInfoRenderer(),
            structlog.processors.format_exc_info,
            structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
        ],
        logger_factory=structlog.stdlib.LoggerFactory(),
        wrapper_class=structlog.stdlib.BoundLogger,
    )

    formatter = structlog.stdlib.ProcessorFormatter(
        # the version of structlog on debian is too old to automatically use colors
        processor=structlog.dev.ConsoleRenderer(colors=False),
        foreign_pre_chain=[
            structlog.stdlib.add_log_level,
            structlog.stdlib.add_logger_name,
        ]
    )

    if use_syslog:
        from logging.handlers import SysLogHandler

        handler = SysLogHandler(address='/dev/log')  # type: logging.Handler
    else:
        handler = logging.StreamHandler()


    handler.setFormatter(formatter)
    root_logger = logging.getLogger()
    root_logger.addHandler(handler)
    root_logger.setLevel(loglevel)


async def run_server() -> None:
    prctl.set_name('odrd')
    prctl.set_proctitle(' '.join(sys.argv))

    parser = OptionParser()
    parser.add_option(
        "-c",
        "--config",
        dest="config_file",
        help="Configuration file",
        default=CONFIG_FILE,
    )
    parser.add_option(
        "--debug",
        dest="debug",
        action="store_true",
        help="Activate debug logging",
        default=False,
    )
    parser.add_option(
        "--keep-user",
        dest="keep_user",
        action="store_true",
        help="Do not switch to a different UID / GID; ignore capabilities",
        default=False,
    )
    (options, args) = parser.parse_args()
    if len(args) != 0:
        parser.error("incorrect number of arguments")

    cfg = ConfigParser()
    cfg.read(options.config_file)

    loglevel = logging.INFO
    if options.debug:
        loglevel = logging.DEBUG
    setup_logging(loglevel, cfg.getboolean('daemon', 'syslog', fallback=False))

    prom_port = cfg.getint("daemon", "prometheus_port", fallback=0)
    if prom_port:
        logging.debug("starting prometheus exporter on port %s", prom_port)
        start_http_server(prom_port)

    if not options.keep_user:
        # Capability net_raw is needed for binding to network devices.
        # Capability net_bind_service is needed for binding to the DHCP port.
        drop_caps(
            user=cfg.get('daemon', 'user', fallback=None),
            group=cfg.get('daemon', 'group', fallback=None),
            caps=['net_raw', 'net_bind_service'],
        )

    default_dhcp_device = cfg.get(
        'daemon', 'default_dhcp_listening_device', fallback=None
    )

    realms_data = read_realms(cfg, default_dhcp_device)
    if realms_data is None:
        sys.exit(1)

    sloop = SocketLoop()

    def exit_daemon(*args: Any) -> None:
        """Signal handler performing a soft shutdown of the loop.
        """
        logging.info('exiting on signal')
        sloop.quit()

    signal.signal(signal.SIGTERM, exit_daemon)
    signal.signal(signal.SIGHUP, signal.SIG_IGN)

    requestor_mgr = odr.dhcprequestor.DhcpAddressRequestorManager()

    servers = read_servers(cfg, sloop)

    def start_dhcp_address_request(device: str, local_ip: str, **kwargs: Any) -> "Future[DhcpAddressResponse]":
        requestor = requestor_mgr.get_requestor(device, local_ip)
        request = odr.dhcprequestor.DhcpAddressInitialRequest(
            requestor=weakref.proxy(requestor),
            local_ip=local_ip,
            **kwargs
        )
        return requestor.send_request(request)

    def start_dhcp_refresh_request(device: str, local_ip: str, **kwargs: Any) -> "Future[DhcpAddressResponse]":
        requestor = requestor_mgr.get_requestor(device, local_ip)
        request = odr.dhcprequestor.DhcpAddressRefreshRequest(
            requestor=weakref.proxy(requestor),
            local_ip=local_ip,
            **kwargs
        )
        return requestor.send_request(request)

    parse_username = ParseUsername(default_realm=cfg.get('daemon', 'default_realm'))

    client_mgr = OvpnClientManager(
        servers=servers,
        secret=cfg.get('daemon', 'secret'),
        refresh_lease_clb=start_dhcp_refresh_request,
        send_request_clb=start_dhcp_address_request,
        realms_data=realms_data,
        parse_username_clb=parse_username.parse_username,
    )

    def create_vpn_cmd_conn(sloop: SocketLoop, sock: socket.socket) -> OvpnCmdConn:
        return OvpnCmdConn(
            sloop,
            sock,
            realms_data=realms_data,
            servers=servers,
            client_mgr=client_mgr,
            parse_username_clb=parse_username.parse_username,
        )

    cmd_socket_uids = [
        user_to_uid(user)
        for user in split_cfg_list(cfg.get('daemon', 'cmd_socket_uids', fallback=''))
    ]
    cmd_socket_gids = [
        group_to_gid(group)
        for group in split_cfg_list(cfg.get('daemon', 'cmd_socket_gids', fallback=''))
    ]

    def cmd_conn_auth_check(sock: socket.socket, pid: int,
                            uid: int, gid: int) -> bool:
        if uid in cmd_socket_uids:
            return True
        if gid in cmd_socket_gids:
            return True
        return False

    cmd_socket_perms = int(cfg.get('daemon', 'cmd_socket_perms', fallback='0666'), 8)
    for unix_socket_fn in split_cfg_list(cfg.get('daemon', 'cmd_sockets', fallback='')):
        cmd_listener = CommandConnectionListener(
            sloop=weakref.proxy(sloop),
            socket_path=unix_socket_fn,
            cmd_conn_factory=create_vpn_cmd_conn,
            socket_perm_mode=cmd_socket_perms,
            auth_check=cmd_conn_auth_check,
        )
        sloop.add_socket_handler(cmd_listener)

    if not load_requestors(sloop, requestor_mgr, realms_data):
        sys.exit(1)

    if not options.keep_user:
        # Special capabilities no longer necessary.
        drop_caps()

    svs = [ovpn.supervise_server(server=weakref.proxy(s), timeout=30) for s in servers.values()]
    try:
        await asyncio.gather(*svs)
    except Exception:
        logging.exception('Caught exception in main loop, exiting.')
        sys.exit(1)

def main():
    asyncio.run(run_server())

if __name__ == '__main__':
    main()
