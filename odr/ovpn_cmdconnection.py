import os
import logging
import socket
from typing import Optional, TextIO, List, Dict, Callable
from asyncio import Future
import asyncio

from prometheus_client import Counter

from .cmdconnection import CommandConnection
from .realmdata import RealmData
from .socketloop import SocketLoop
from . import ovpn
from .ovpn_client_mgr import OvpnClientManager
from .ovpn_config import OvpnConf
from .ovpn_client import OvpnClient

M_OVPN_RET_SUCCESS_COUNT = Counter(
    "odr_ovpn_ret_sucess_count", "number of successes sent to ovpn"
)
M_OVPN_RET_FAILED_COUNT = Counter(
    "odr_ovpn_ret_failed_count", "number of failures sent to ovpn"
)

class OvpnCmdConn(CommandConnection):
    """Represents an incoming command connection from one of the OpenVPN
    hooks.
    """

    def __init__(
        self,
        sloop: SocketLoop,
        sock: socket.socket,
        realms_data: Dict[str, RealmData],
        servers: Dict[str, ovpn.OvpnServer],
        parse_username_clb: Callable[[], None],
        client_mgr: OvpnClientManager,
    ) -> None:
        """\
        @param sloop: Socket loop instance.  (See CommandConnection for
            details.)
        @param sock: Socket of the command connection.  (See CommandConnection
            for details.)
        @param realms_data: Dictionary of realms data objects.  Indexed by
            realm name.
        @param servers: Dictionary of servers.  Indexed by server name.
        @param send_request_clb: Call-back for starting an initial DHCP request.
        @param parse_username_clb: Call-back for parsing a full username into
            the components.
        @param create_client_clb: Call-back for creating and registering a new
            OpenVPN client instance.
        @param remove_client_clb: Call-back for removing an existing OpenVPN
            client instance.
        """
        CommandConnection.__init__(
            self, sloop=sloop, sock=sock, log=logging.getLogger('ovpncmdconn')
        )
        self._realms_data = realms_data
        self._servers = servers
        self._parse_username = parse_username_clb
        self._client_mgr = client_mgr
        self._ret_f = None
        self._wrote_ret = False
        self._config_f = None  # type: Optional[TextIO]
        self._full_username = None
        self._server = None
        self._realm_data = None  # type: Optional[RealmData]

    def __del__(self) -> None:
        self._log.debug('destructing OvpnCmdConn')
        if self._ret_f is not None:
            if not self._wrote_ret:
                self._write_ret_failed()
            self._ret_f.close()
        if self._config_f is not None:
            self._config_f.close()
        CommandConnection.__del__(self)

    def _write_ret(self, val) -> None:
        """Write a specific return value to the deferred return value file.
        @param val: A CC_RET_* value.
        """
        self._log.debug('writing deferred return value %d', val)
        assert self._ret_f is not None
        ovpn.write_deferred_ret_file(self._ret_f, val)
        self._wrote_ret = True

    def _write_ret_success(self) -> None:
        """Write a success value to the deferred return value file."""
        M_OVPN_RET_SUCCESS_COUNT.inc()
        self._write_ret(ovpn.CC_RET_SUCCEEDED)

    def _write_ret_failed(self) -> None:
        """Write a success value to the deferred return value file."""
        M_OVPN_RET_FAILED_COUNT.inc()
        self._write_ret(ovpn.CC_RET_FAILED)

    def handle_cmd(self, cmd: str, params: Dict[str, str], files: List[int]) -> None:
        """Called for each command received over the command socket.  Forwards
        the command processing according to the command's name.

        @param cmd: Name of the command.
        @param params: Dictionary of the command's parameters.
        @param files: Array of file pointers passed through with the command.
        """
        if cmd == 'request':
            self._handle_request_cmd(cmd, params, files)
        elif cmd == 'disconnect':
            self._handle_disconnect_cmd(cmd, params, files)
        else:
            self.send_cmd('FAIL')
            self._log.warning('received unknown command "%s"', cmd)
            return

    def _handle_request_cmd(self, cmd: str, params: Dict[str, str], files: List[int]) -> None:
        """Handles the command for sending an initial DHCP address request.
        """
        try:
            self._full_username = params['full_username']
            ret_fidx = params['ret_file_idx']
            config_fidx = params['config_file_idx']
            server_name = params['daemon_name']
        except KeyError as exc:
            self.send_cmd('FAIL')
            self._log.warning('command "%s" is missing a parameter: %s', cmd, exc.args)
            return

        try:
            ret_f = files[int(ret_fidx)]
            config_f = files[int(config_fidx)]
        except IndexError as exc:
            self.send_cmd('FAIL')
            self._log.warning('file descriptor index out of range: %s', exc.args)
            return
        except ValueError as exc:
            self.send_cmd('FAIL')
            self._log.warning('file descriptor index parsing failed: %s', exc.args)
            return

        ret = self._parse_username(self._full_username)
        if ret is None:
            self.send_cmd('FAIL')
            self._log.warning('parsing username failed: "%s"', self._full_username)
            return
        realm = ret['realm']

        if realm not in self._realms_data:
            self.send_cmd('FAIL')
            self._log.error('unknown realm "%s"', realm)
            return

        if server_name not in self._servers:
            self.send_cmd('FAIL')
            self._log.error('unknown server "%s"', server_name)
            return

        realm_data = self._realms_data[realm]
        self._realm_data = realm_data
        self._server = self._servers[server_name]

        self.send_cmd('OK')
        self._ret_f = ret_f
        self._config_f = config_f

        fut = asyncio.ensure_future(self._create_client())
        fut.add_done_callback(self._handle_request_done)

    def _handle_request_done(self, fut: "Future[None]") -> None:
        try:
            fut.result()
        except Exception as e:
            self._log.exception('Failed to create client')
            self._write_ret_failed()

    async def _create_client(self):
        client = self._client_mgr.create_client(
            full_username=self._full_username,
            server=self._server,
            realm_data=self._realm_data,
        )

        try:
            conf = await client.generate_config()
            # self._log.debug('DHCP request succeeded: %s', repr(res))
        except Exception as e:
            self._write_ret_failed()
            return

        self._log.debug('writing OpenVPN client configuration')
        assert self._config_f is not None

        self._config_f.seek(0)
        self._config_f.write(conf.to_text())
        self._config_f.flush()
        os.fsync(self._config_f.fileno())

        self._write_ret_success()
        client.track_lease()

    def _handle_disconnect_cmd(self, cmd: str, params: Dict[str, str], files: List[int]) -> None:
        """Handles the command for informing us of a client disconnect.
        """
        try:
            full_username = params['full_username']
            server_name = params['daemon_name']
        except KeyError as exc:
            self.send_cmd('FAIL')
            self._log.warning('command "%s" is missing a parameter: %s', cmd, exc.args)
            return

        if server_name not in self._servers:
            self.send_cmd('FAIL')
            self._log.error('unknown server "%s"', server_name)
            return
        server = self._servers[server_name]

        self.send_cmd('OK')
        self._client_mgr.client_disconnected(full_username, server)


