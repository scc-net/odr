"""
utility for easily creating and inspecting openvpn client configs
"""

from itertools import chain
from typing import Iterable, Tuple, List
import datetime
import hashlib
from ipaddress import IPv6Network

from .realmdata import RealmData
from .dhcprequestor import DhcpAddressResponse

def config_escape(option: str) -> str:
    """escape a single value as required by openvpn"""
    # escape backslashes
    option = option.replace("\\", "\\\\")

    # quote if necessary
    if any(c.isspace() for c in option):
        return '"{}"'.format(option.replace('"', '\\"'))

    return option


def make_config_line(values: Iterable[str]) -> str:
    return " ".join(config_escape(val) for val in values)


class OvpnConf:
    """construct an openvpn config"""

    def __init__(self) -> None:
        self.lines: List[str] = []

    def add(self, *values: str) -> None:
        """add one config line"""
        self.lines.append(make_config_line(values))

    def push(self, *values: str) -> None:
        """add one one config line to push to the client"""
        self.add("push", make_config_line(values))

    def push_dhcp_option(self, option: str, *values: str) -> None:
        """add one one dhcp option to push to the client"""
        self.push("dhcp-option", option.upper(), *values)

    def to_text(self) -> str:
        """get the current config as a string"""
        return "\n".join(self.lines)

def add_realm_config(conf: OvpnConf, realm: RealmData) -> None:
    if realm.vid is not None:
        conf.add("vlan-pvid", str(realm.vid))
    conf.push("redirect-private")

    if realm.dns_resolvers:
        for dns_ip in realm.dns_resolvers:
            conf.push_dhcp_option("DNS", dns_ip)

    if realm.dns_domain:
        conf.push_dhcp_option("DOMAIN", realm.dns_domain)

    if realm.dns_domain_search:
        for domain in realm.dns_domain_search:
            conf.push_dhcp_option("DOMAIN-SEARCH", domain)

def add_ipv6_config(conf: OvpnConf, realm: RealmData, user_hash: bytes) -> None:
    assert realm.subnet_ipv6 is not None
    if len(user_hash) != 8:
        raise ValueError("user has needs to be 64 bits")
    prefix = realm.subnet_ipv6

    ipv6_network = IPv6Network(prefix).network_address
    ip6_address = str(ipv6_network + int.from_bytes(user_hash, "big"))

    if realm.default_gateway_ipv6 is not None:
        ip6_gateway = realm.default_gateway_ipv6
    else:
        ip6_gateway = str(ipv6_network + 1)

    conf.add("ifconfig-ipv6-push", ip6_address, ip6_gateway)

    if realm.provide_default_route:
        if realm.default_gateway_ipv6 is not None:
            conf.push("route-ipv6", "2000::/3")
            conf.push("redirect-gateway", "def1")
    else:
        static_routes_ipv6 = []  # type: List[Tuple[str, str]]
        if (
            realm.subnet_ipv6 is not None
            and realm.static_routes_ipv6 is not None
        ):
            static_routes_ipv6 += realm.static_routes_ipv6
        for network, gateway in static_routes_ipv6:
            conf.push("route-ipv6", network, gateway)


def add_ipv4_config(conf: OvpnConf, realm: RealmData, res: DhcpAddressResponse) -> None:
    conf.add("ifconfig-push", res.ip_address, res.subnet_mask)

    if realm.default_gateway_ipv4 is not None:
        conf.push("route-gateway", realm.default_gateway_ipv4)
    elif res.gateway:
        conf.push("route-gateway", res.gateway)
    else:
        RuntimeError('DHCP request provided no gateway information: %s' % (repr(res)))

    if realm.provide_default_route:
        if res.gateway or realm.default_gateway_ipv4 is not None:
            conf.push("redirect-gateway", "def1")
    else:
        static_routes_ipv4 = []  # type: List[Tuple[str, str, str]]
        if realm.static_routes_ipv4 is not None:
            static_routes_ipv4 += realm.static_routes_ipv4
        elif res.static_routes:
            static_routes_ipv4 += res.static_routes
        if len(static_routes_ipv4) > 0:
            for network, netmask, gateway in static_routes_ipv4:
                conf.push("route", network, netmask, gateway)

    if not realm.dns_resolvers:
        for dns_ip in res.dns:
            conf.push_dhcp_option("DNS", dns_ip)

    if not realm.dns_domain:
        if res.domain:
            conf.push_dhcp_option("DOMAIN", res.domain)
