"""Realmdata stores the static configuration data of a realm"""

from configparser import ConfigParser, SectionProxy
from dataclasses import dataclass
import logging
import socket

from typing import Dict, Optional, List, Tuple

from .config import (
    cfg_iterate,
    parse_static_routes_ipv4,
    parse_static_routes_ipv6,
    split_cfg_list,
)
RouteListV4 = List[Tuple[str, str, str]]
RouteListV6 = List[Tuple[str, str]]

@dataclass
class RealmData:
    """A RealmData object contains all data relevant for a specific realm.
    The attributes are injected at configuration-load-time.
    """
    name: str
    vid: Optional[int] = None
    dhcp_local_port = 67
    dhcp_listening_device: Optional[str] = None
    dhcp_listening_ip: Optional[str] = None
    provide_default_route = True
    default_gateway_ipv4: Optional[str] = None
    subnet_ipv6: Optional[str] = None
    subnet_ipv4: Optional[str] = None
    default_gateway_ipv6: Optional[str] = None
    static_routes_ipv4: Optional[RouteListV4] = None
    static_routes_ipv6: Optional[RouteListV6] = None
    dhcp_server_ips: Optional[List[str]] = None
    expected_dhcp_lease_time: Optional[int] = None
    dns_domain: Optional[str] = None
    dns_resolvers: Optional[List[str]] = None
    dns_domain_search: Optional[List[str]] = None


def get_ip_for_iface(iface: str) -> str:
    """Look up an IPv4 address on the given interface.
    @param iface: Interface name
    @return: Returns the IPv4 address as string.
    """
    import netifaces

    addrs = netifaces.ifaddresses(iface)
    if (
        netifaces.AF_INET not in addrs
        or len(addrs[netifaces.AF_INET]) == 0
        or 'addr' not in addrs[netifaces.AF_INET][0]
    ):
        raise RuntimeError('Could not detect IPv4 address on interface "%s"' % iface)
    return addrs[netifaces.AF_INET][0]['addr']


def process_realm(
    cfg: SectionProxy, realm_name: str, realms: Dict[str, RealmData],
) -> RealmData:
    """take a realm config section and turn it into RealmData, resolving
    interfaces and addresses as needed"""
    logging.debug('processing realm "%s"', realm_name)
    realm_data = RealmData(realm_name)

    realm_data.vid = cfg.getint('vid', fallback=realm_data.vid)

    realm_data.dhcp_local_port = cfg.getint(
        'dhcp_local_port', fallback=realm_data.dhcp_local_port
    )

    if 'dhcp_listening_device' in cfg:
        realm_data.dhcp_listening_device = cfg.get('dhcp_listening_device')
        # If a device is explicitly set, the listening IP needs to be explicitly
        # set too (or the implicit detection needs to be performed again).
        realm_data.dhcp_listening_ip = None

    realm_data.dhcp_listening_ip = cfg.get(
        'dhcp_listening_ip', fallback=realm_data.dhcp_listening_ip
    )

    realm_data.provide_default_route = cfg.getboolean(
        'provide_default_route', fallback=realm_data.provide_default_route
    )

    realm_data.default_gateway_ipv4 = cfg.get(
        'default_gateway_ipv4', fallback=realm_data.default_gateway_ipv4
    )

    realm_data.subnet_ipv6 = cfg.get('subnet_ipv6', fallback=realm_data.subnet_ipv6)

    realm_data.subnet_ipv4 = cfg.get('subnet_ipv4', fallback=realm_data.subnet_ipv4)

    realm_data.default_gateway_ipv6 = cfg.get(
        'default_gateway_ipv6', fallback=realm_data.default_gateway_ipv6
    )

    if 'static_routes_ipv4' in cfg:
        realm_data.static_routes_ipv4 = parse_static_routes_ipv4(
            cfg.get('static_routes_ipv4')
        )

    if 'static_routes_ipv6' in cfg:
        realm_data.static_routes_ipv6 = parse_static_routes_ipv6(
            cfg.get('static_routes_ipv6')
        )

    if (
        realm_data.dhcp_listening_device is not None
        and realm_data.dhcp_listening_ip is None
    ):
        # We need to determine an IPv4 address on the specified network
        # device.
        realm_data.dhcp_listening_ip = get_ip_for_iface(
            realm_data.dhcp_listening_device
        )

    if 'dhcp_server_ips' in cfg:
        realm_data.dhcp_server_ips = [
            socket.gethostbyname(i.strip())
            for i in cfg.get('dhcp_server_ips').split(',')
        ]

    realm_data.dns_domain = cfg.get('dns_domain', fallback=realm_data.dns_domain)

    if 'dns_resolvers' in cfg:
        # this does not resolve the name, however right now we are only passing
        # the dns resolvers by IP
        realm_data.dns_resolvers = [
            i.strip() for i in cfg.get('dns_resolvers').split(',')
        ]

    if 'dns_domain_search' in cfg:
        realm_data.dns_domain_search = [
            i.strip() for i in cfg.get('dns_domain_search').split(',')
        ]

    realm_data.expected_dhcp_lease_time = cfg.getint(
        'expected_dhcp_lease_time', fallback=realm_data.expected_dhcp_lease_time
    )

    return realm_data


def read_realms(
    cfg: ConfigParser, default_dhcp_device: str = None
) -> Optional[Dict[str, RealmData]]:
    """Read all realms from the configuration file into a dictionary of
    RealmData objects.
    """
    realms = {}  # type: Dict[str, RealmData]

    for _, realm_name in cfg_iterate(cfg, 'realm'):
        sect = 'realm ' + realm_name

        # this is the least ugly way to do this, sadly
        if default_dhcp_device and "dhcp_listening_device" not in cfg[sect]:
            cfg[sect]["dhcp_listening_device"] = default_dhcp_device

        realm = process_realm(cfg[sect], realm_name, realms)
        realms[realm_name] = realm
    return realms
