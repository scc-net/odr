from odr.parse import ParseUsername

def test_parse():
    parse = ParseUsername("default")
    res = parse.parse_username("user/resource@realm")
    assert res == {
        "username": "user",
        "resource": "resource",
        "realm": "realm",
        "domain": None,
    }
