from odr.ovpn_config import OvpnConf, config_escape, add_ipv6_config
from odr.realmdata import RealmData

def test_config_escape():
    # unchanged
    assert config_escape("test") == 'test'
    # quote if contains spaces
    assert config_escape("test 123") == '"test 123"'
    # escape quotes if quoting
    assert config_escape("test \" 123") == '"test \\" 123"'
    # escape backslash always
    assert config_escape("te\\st") == 'te\\\\st'
    assert config_escape("test te\\st") == '"test te\\\\st"'


def test_simple_conf():
    conf = OvpnConf()
    conf.add("test", "1", "2")
    conf.push("test", "1", "2")
    expected = 'test 1 2\npush "test 1 2"'
    assert conf.to_text() == expected

def test_add_ipv6_config():
    conf = OvpnConf()
    realm = RealmData(
        name="test",
    )
    realm.subnet_ipv6 = "FFFF::"
    add_ipv6_config(conf, realm=realm, user_hash=b"01234567")
    assert conf.to_text() == "ifconfig-ipv6-push ffff::3031:3233:3435:3637 ffff::1"

def test_add_ipv6_config_routes():
    conf = OvpnConf()
    realm = RealmData(
        name="test",
    )
    realm.subnet_ipv6 = "FFFF::"
    realm.provide_default_route = True
    realm.default_gateway_ipv6 = "ffff::5"
    add_ipv6_config(conf, realm=realm, user_hash=b"01234567")
    cfg_expected = "\n".join([
        "ifconfig-ipv6-push ffff::3031:3233:3435:3637 ffff::5",
        'push "route-ipv6 2000::/3"',
        'push "redirect-gateway def1"',
    ])
    assert conf.to_text() == cfg_expected
